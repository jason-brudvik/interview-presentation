/*global $*/
'use strict';

// The gloabl variables for this applicaiton
var MAIN_APP = {

    buttonConfig : [

        // HDF5
        {   name    : 'FULL STACK SOFTWARE ENGINEER 2024-06-19',
            color   : 'white',
            page    : '../2024-06-19-ESS-Full-Stack-Software-Engineer/index.html',
            div     : 'divHDFrWorkshop',
            icon    : 'glyphicon-cloud-download',
            xsShow  : true,
            group   : 'ess'},
    ]
};

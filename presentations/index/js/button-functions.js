/*global $*/
'use strict';

// External and gloabl variables and functions for this applicaiton
var MAIN_APP, BUTT_FUNC = {

    showAll : true,

    // A function for creating divs for each button
    createButtons : function () {

        var outputHTML = '', i_butt, buttonConfig, divClass;

        // Loop over the button configurations
        for (i_butt = 0; i_butt < MAIN_APP.buttonConfig.length; i_butt += 1) {

            buttonConfig = MAIN_APP.buttonConfig[i_butt];

            // Set the button size
            divClass = 'col-xs-6 col-md-4 col-lg-4 top-buffer ';

            // Set whether the button is hidden by default in xs
            if (!buttonConfig.xsShow) {
                divClass += ' hidden-xs';
            }

            // The complete html for this button
            outputHTML += '<div class="' + divClass + '" ' +
                          '" id="' + buttonConfig.div + '" >' +
                          '<a href="' + buttonConfig.page + '" ' +
                          'class="btn btn-lg btn-block btn-inverse ' +
                          'btn-lg-text-sm text-left"> ' +
                          '<span class="glyphicon ' +
                          buttonConfig.icon + '"> </span> ' +
                          '<span class="' + buttonConfig.color + '">' +
                          buttonConfig.name + '</span> </a>' +
                          '</div>';
        }

        // Add all the button html code to the page
        $('#pageListDiv').html(outputHTML);
    },


    toggleSingleButton : function (divTag, toggleButton, forceShow) {

        var showButton = false;

        if (toggleButton) {
            // If this button is currently hidden, show it
            if ($(divTag).hasClass('hidden-md')) {
                showButton = true;
            } else {
                showButton = false;
            }
        } else {
            showButton = false;
        }

        // For showing or hiding all buttons
        if (forceShow) {
            showButton = toggleButton;
        }

        // Change buttons classes to hidden or not, except for xs screens
        if (showButton) {
            $(divTag).removeClass('hidden-sm hidden-md hidden-lg');
        } else {
            $(divTag).addClass('hidden-sm hidden-md hidden-lg');
        }
    },


    toggleAllButtons : function () {

        var debug = false, i_butt, buttonConfig;

        // Toggle visibility for all buttons
        BUTT_FUNC.showAll = !BUTT_FUNC.showAll;

        // Loop over the button configurations
        for (i_butt = 0; i_butt < MAIN_APP.buttonConfig.length; i_butt += 1) {

            buttonConfig = MAIN_APP.buttonConfig[i_butt];

            // Show or hide each button (except for xs display)
            BUTT_FUNC.toggleSingleButton('#' + buttonConfig.div,
                BUTT_FUNC.showAll, true);
        }

        if (debug) {
            console.log('BUTT_FUNC.showAll: ' + BUTT_FUNC.showAll);
        }
    },


    toggleButtons : function (buttonClicked) {

        var debug = false, i_butt, buttonConfig = null;

        // If all buttons are visible, toggle them off
        if (BUTT_FUNC.showAll) {
            BUTT_FUNC.toggleAllButtons();
        }

        // Loop over the button configurations
        for (i_butt = 0; i_butt < MAIN_APP.buttonConfig.length; i_butt += 1) {

            buttonConfig = MAIN_APP.buttonConfig[i_butt];

            // Show only buttons in the selected group
            BUTT_FUNC.toggleSingleButton('#' + buttonConfig.div,
                buttonConfig.group === buttonClicked, false);
        }

        if (debug) {
            console.log('BUTT_FUNC.showAll: ' + BUTT_FUNC.showAll);
        }
    },


};

// This function fires when the page is loaded, the first call to the data
// request function is made
$(document).ready(function () {

    var debug = false;

    // Create all the buttons
    BUTT_FUNC.createButtons();

    // Hide them all by default except for xs devices
    BUTT_FUNC.toggleAllButtons();

    // Call window resize just for debugging
    if (debug) {
        $(window).resize();
    }
});


// For debugging - see what size the screen is
$(window).resize(function () {

    var debug = false, appWidth, viewingDeviceSize = 'xs';

    // $(document) refers to the document size
    // $(window) refers to the browser size
    // screen gives screen size info - might be good to check that I am not
    // outside this size

    if (debug) {
        // Calculate the new chart width
        appWidth = document.getElementById('applicationContainer').offsetWidth;

        console.log('appWidth:' + appWidth);
        console.log('window height:' + $(window).height());

        // Try to figure out what kind of device (phone, desktop) is being
        // used.
        if ($(window).width() > 1200) {
            viewingDeviceSize = 'lg';
        } else if ($(window).width() <= 1200 && $(window).width() > 992) {
            viewingDeviceSize = 'md';
        } else if ($(window).width() <= 992 && $(window).width() > 768) {
            viewingDeviceSize = 'sm';
        } else if ($(window).width() <= 768) {
            viewingDeviceSize = 'xs';
        }

        console.log('viewingDeviceSize: ' + viewingDeviceSize);
    }
});
